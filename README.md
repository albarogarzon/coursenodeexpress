# Curso Desarrollo del lado servidor: NodeJS, Express y MongoDB

"Te invitamos a demostrar tus aprendizajes adquiridos hasta ahora en el curso. Se trata del producto de llevar a cabo las guías prácticas de los módulos."

```bash
- Crear un proyecto de Node JS.
- Resguardar tu proyecto con GIT sobre Bitbucket.
- Configurar librerías con NPM.
- Aplicar los conceptos básicos de la programación web y Express.
```
## Módulo 2